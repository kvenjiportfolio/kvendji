$(document).ready(function () {


    const owl = $('#some-carousel');

    owl.owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            988: {
                items: 3
            }
        }
    });

    $('#next_item_carousel').click(function () {
        owl.trigger('next.owl.carousel');
    });

    $('#prev_item_carousel').click(function () {

        owl.trigger('prev.owl.carousel', [300]);
    });


    const owl_second = $('#about_us_carousel');

    owl_second.owlCarousel({
        loop:true,
        margin:10,
        autoWidth:false,
        nav:true,
        responsive:{
            0:{
                items:1
            }}
    });

    $('#next_item_carousel_about_us').click(function() {
        owl_second.trigger('next.owl.carousel');
    });

    $('#prev_item_carousel_about_us').click(function() {

        owl_second.trigger('prev.owl.carousel', [300]);
    });


    $('.catalog').click(function () {
        $('.menu_services').slideDown(3000);
    });


});




